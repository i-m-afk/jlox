package src.lox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Lox {

    static boolean hadError = false;

    static boolean hadRuntimeError = false;

    static void error(int line, String message) {
        report(line, "", message);
    }

    // report error

    private static void report(int line, String where, String message) {
        System.err.println("[line " + line + "] Error" + where + ": " + message);
        hadError = true;
    }
    private static void run(String source) {
        Scanner scanner = new Scanner(source);
        List<Token> tokens = scanner.scanTokens();
        for (Token token : tokens) {
            System.out.println(token);
        }
    }

    private static void runPrompt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        while (true) {
            System.out.print("> ");
            try {
                String line = reader.readLine();
                if (line == null)
                    break;
                run(line);
                hadError = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void runFile(String path) throws IOException {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            run(new String(bytes, Charset.defaultCharset()));
            if (hadError)
                System.exit(65);
            if (hadRuntimeError)
                System.exit(70);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // main method
    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            System.out.println("Usage: jlox [script]");
            System.exit(64);
        } else if (args.length == 1) {
            runFile(args[0]); // run the file
        } else {
            runPrompt(); // run the repl
        }
    }

}